FROM node:17-alpine
WORKDIR /test
COPY ./backend .
RUN npm install
CMD ["npm","run","e2e"]